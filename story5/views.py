from django.shortcuts import render

def pageone(request):
    return render(request,'pageone.html')

def pagetwo(request):
    return render(request,'pagetwo.html')

def pagethree(request):
    return render(request,'pagethree.html')

def pagefour(request):
    return render(request,'pagefour.html')

def pagefive(request):
    return render(request,'pagefive.html')

def story1(request):
    return render(request, 'story1-ali.html')