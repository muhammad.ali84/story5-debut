from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.pageone, name='pageone'),
    path('pagetwo/', views.pagetwo, name='pagetwo'),
    path('pagethree/', views.pagethree, name='pagethree'),
    path('pagefour/', views.pagefour, name='pagefour'),
    path('pagefive/', views.pagefive, name='pagefive'),
    path('story1/', views.story1, name = 'story1'),
]